import os
import sys
import shutil
import json
import yaml
from datetime import datetime
from sys import exit
import math

from config import *
from workflows.core import *
from flame.workflows import *
from flame.aver_dist import compute_aver_dist
from vasp.workflows import get_vasp_wf
from structure.perturb import compress, expand 

def step_7():
    with open(job_script_file, 'r') as f:
        js = yaml.load(f, Loader=yaml.FullLoader)
    with open(log_file, 'a') as f:
        f.write("---------------------------------------------------------------------------------------------------"+'\n')
        f.write('STEP 7'+'\n')
        f.write('Start time: {}'.format(datetime.now())+'\n')
# calculate the ave_dis for pickdiff if it is not already calculated
    if not os.path.exists(os.path.join(Flame_dir,'ave_dis','ave_dis.json')):
        try:
            shutil.rmtree(os.path.join(Flame_dir,'ave_dis'))
        except:
            pass
        os.mkdir(os.path.join(Flame_dir,'ave_dis'))
        with open(log_file, 'a') as f:
            f.write("-----------------------------------------------"+'\n')
            f.write('Aaverage distance calculation'+'\n') 
            f.write('Start time: {}'.format(datetime.now())+'\n')
#
        if run_exists():
            with open(log_file, 'a') as f:
                f.write('>>> ERROR: unfinished jobs in Launchpad <<<'+'\n')
            exit()
# get average distance wf
        aver_dist_wf = get_aver_dist_wf()
# add workflow
        add_wf(aver_dist_wf)
# run average distance jobs
        if not run_jobs('flame_aver_dist', os.path.join(Flame_dir,'ave_dis')):
            with open(log_file, 'a') as f:
                f.write('>>> Cannot proceed <<<'+'\n')
            exit()
# wait until all jobs are done
        while True:
            if run_exists():
                fizzle_lostruns()
                sleep(60)
            else:
                break
# check launchpad status
        lp_state = check_lp(['flame_aver_dist'])
        if lp_state == 'FIZZLED':
            with open(log_file, 'a') as f:
                f.write('>>> WARNING: Workflow is FIZZLED <<<'+'\n')
        elif lp_state == 'unknown':
            with open(log_file, 'a') as f:
                f.write('>>> Cannot proceed. The state of Workflow is not known.<<<'+'\n')
            exit()
# calculae average distances and store it
        compute_aver_dist()
#
        with open(log_file, 'a') as f:
            f.write('Aaverage distance calculation ended'+'\n')
            f.write('End time: {}'.format(datetime.now())+'\n')

####################FLAME LOOP####################
    FLAME_step_number =     restart['FLAME_step_number']
    FLAME_step_name =       restart['FLAME_step_name']
    FLAME_training_cycles = restart['FLAME_training_cycles']
 
    for c_s_n in range(FLAME_step_number, FLAME_step_number+FLAME_training_cycles):    
        step_number = 'step-'+str(c_s_n)
######FLAME train######
        if FLAME_step_name == 'train':
            with open(log_file, 'a') as f:
                f.write("-----------------------------------------------"+'\n')
                f.write('FLAME-{}: train calculations'.format(step_number)+'\n')
                f.write('Start time: {}'.format(datetime.now())+'\n')
# creating directories
            try:
                os.mkdir(os.path.join(Flame_dir,step_number))
            except FileExistsError:
                pass
            try:
                os.mkdir(os.path.join(Flame_dir,step_number,'train'))
            except FileExistsError:
                with open(log_file, 'a') as f:
                    f.write('>>> Cannot proceed. {} exists'.format(os.path.join(Flame_dir,step_number,'train'))+'\n')
                exit()
#
            if run_exists():
                with open(log_file, 'a') as f:
                    f.write('>>> ERROR: unfinished jobs in Launchpad <<<'+'\n')
                exit()
# get train wf
            flame_train_wf = get_train_wf(step_number)
# add workflow
            add_wf(flame_train_wf)
# run train jobs
            if not run_jobs('train',os.path.join(Flame_dir,step_number,'train')):
                with open(log_file, 'a') as f:
                    f.write('>>> Cannot proceed <<<'+'\n')
                exit()
# wait until all jobs are done
            while True:
                if run_exists():
                    fizzle_lostruns()
                    sleep(60)
                else:
                    break
# check launchpad status
            lp_state = check_lp(['flame_train_'+str(step_number)])
            if lp_state == 'FIZZLED':
                with open(log_file, 'a') as f:
                    f.write('>>> Cannot proceed. Train job is fizzled. <<<'+'\n')
                exit()
            elif lp_state == 'unknown':
                with open(log_file, 'a') as f:
                    f.write('>>> Cannot proceed. The state of Workflow is not known.<<<'+'\n')
                exit()
# a temporrary fix for the wrong format of train_output.yaml
#            for root, dirs, files in os.walk(os.path.join(Flame_dir,step_number,'train')):
#                for f in files:
#                    if f == 'train_output.yaml':
#                        with open(os.path.join(root,'train_output.yaml'), 'r') as f:
#                            old_filedata = f.read()
#                        new_filedata_1 = old_filedata.replace('train:',' - train:')
#                        new_filedata_2 = new_filedata_1.replace('valid:',' - valid:')
#                        with open(os.path.join(Flame_dir,step_number,"train",'train_output.yaml'), 'w') as f:
#                            f.write(new_filedata_2)
# end of training?
            if c_s_n == FLAME_step_number+FLAME_training_cycles-1:
                with open(log_file, 'a') as f:
                    f.write('{}: END OF THE TRAINING CYCLE. BYE'.format(step_number)+'\n')
                    f.write('End time: {}'.format(datetime.now())+'\n')
                    f.write("----------------------------------------------------------------------------------------------"+'\n')
                exit()
#
            with open(log_file, 'a') as f:
                f.write('FLAME-{}: train calculations ended.'.format(step_number)+'\n')
                f.write('End time: {}'.format(datetime.now())+'\n')
#
            FLAME_step_name = 'minhocao'
######FLAME minhocao######
        if FLAME_step_name == 'minhocao':
            with open(log_file, 'a') as f:
                f.write("-----------------------------------------------"+'\n')
                f.write('FLAME-{}: minhocao calculations'.format(step_number)+'\n')
                f.write('Start time: {}'.format(datetime.now())+'\n')
# creating directories
            try:
                os.mkdir(os.path.join(Flame_dir,step_number,'minhocao'))
            except FileExistsError:
                with open(log_file, 'a') as f:
                    f.write('>>> Cannot proceed. {} exists <<<'.format(os.path.join(Flame_dir,step_number,'minhocao'))+'\n')
                exit()
            if run_exists():
                with open(log_file, 'a') as f:
                    f.write('>>> ERROR: unfinished jobs in Launchpad <<<'+'\n')
                exit()
# get train wf
            flame_minhocao_wf = get_minhocao_wf(step_number)
# add workflow
            add_wf(flame_minhocao_wf)
# run minhocao jobs
            if not run_jobs('minhocao',os.path.join(Flame_dir,step_number,'minhocao')):
                with open(log_file, 'a') as f:
                    f.write('>>> Cannot proceed <<<'+'\n')
                exit()
# wait until all jobs are done
            while True:
                if run_exists():
                    fizzle_lostruns()
                    sleep(60)
                else:
                    break
# check launchpad status
            lp_state = check_lp(['flame_minhocao_'+str(step_number)])
            if lp_state == 'FIZZLED':
                with open(log_file, 'a') as f:
                    f.write('>>> WARNING: Workflow is FIZZLED <<<'+'\n')
            elif lp_state == 'unknown':
                with open(log_file, 'a') as f:
                    f.write('>>> Cannot proceed. The state of Workflow is not known.<<<'+'\n')
                exit()
            with open(log_file, 'a') as f:
                f.write('FLAME-{}: minhocao calculations ended.'.format(step_number)+'\n')
                f.write('End time: {}'.format(datetime.now())+'\n')
#
            if input_list['cluster_calculation']:
                FLAME_step_name = 'minhopp' 
            else:
                FLAME_step_name = 'divcheck'
######FLAME minhopp######
        if FLAME_step_name == 'minhopp':
            with open(log_file, 'a') as f:
                f.write("-----------------------------------------------"+'\n')
                f.write('FLAME-{}: minhopp calculations'.format(step_number)+'\n')
                f.write('Start time: {}'.format(datetime.now())+'\n')
# creating directories
            try:
                os.mkdir(os.path.join(Flame_dir,step_number,'minhopp'))
            except FileExistsError:
                with open(log_file, 'a') as f:
                    f.write('>>> Cannot proceed. {} exists <<<'.format(os.path.join(Flame_dir,step_number,'minhopp'))+'\n')
                exit()
            if run_exists():
                with open(log_file, 'a') as f:
                    f.write('>>> ERROR: unfinished jobs in Launchpad <<<'+'\n')
                exit()
# get train wf
            flame_minhopp_wf = get_minhopp_wf(step_number)
# add workflow
            add_wf(flame_minhopp_wf)
# run minhopp jobs
            if not run_jobs('minhopp',os.path.join(Flame_dir,step_number,'minhopp')):
                with open(log_file, 'a') as f:
                    f.write('>>> Cannot proceed <<<'+'\n')
                exit()
# wait until all jobs are done
            while True:
                if run_exists():
                    fizzle_lostruns()
                    sleep(60)
                else:
                    break
# check launchpad status
            lp_state = check_lp(['flame_minhopp_'+str(step_number)])
            if lp_state == 'FIZZLED':
                with open(log_file, 'a') as f:
                    f.write('>>> WARNING: Workflow is FIZZLED <<<'+'\n')
            elif lp_state == 'unknown':
                with open(log_file, 'a') as f:
                    f.write('>>> Cannot proceed. The state of Workflow is not known.<<<'+'\n')
                exit()
#
            with open(log_file, 'a') as f:
                f.write('FLAME-{}: minhopp calculations ended.'.format(step_number)+'\n')
                f.write('End time: {}'.format(datetime.now())+'\n')
#
            FLAME_step_name = 'divcheck'
######FLAME divcheck######
        if FLAME_step_name == 'divcheck':
            with open(log_file, 'a') as f:
                f.write("-----------------------------------------------"+'\n')
                f.write('FLAME-{}: divcheck calculations'.format(step_number)+'\n')
                f.write('Start time: {}'.format(datetime.now())+'\n')
# creating directories
            try:
                os.mkdir(os.path.join(Flame_dir,step_number,'divcheck'))
            except FileExistsError:
                pass
# for bulks
            try:
                os.mkdir(os.path.join(Flame_dir,step_number,'divcheck','bulk'))
            except FileExistsError:
                with open(log_file, 'a') as f:
                    f.write('>>> Cannot proceed. {} exists'.format(os.path.join(Flame_dir,step_number,'divcheck','bulk'))+'\n')
                exit()
            if run_exists():
                with open(log_file, 'a') as f:
                    f.write('>>> ERROR: unfinished jobs in Launchpad <<<'+'\n')
                exit()
# get divcheck workflow
            flame_divcheck_b_wf = get_divcheck_b_wf(step_number)
# add workflow
            add_wf(flame_divcheck_b_wf)
# run train jobs
            if not run_jobs('divcheck-bulk',os.path.join(Flame_dir,step_number,'divcheck','bulk')):
                with open(log_file, 'a') as f:
                    f.write('>>> Cannot proceed <<<'+'\n')
                exit()
# wait until all jobs are done
            while True:
                if run_exists():
                    fizzle_lostruns()
                    sleep(60)
                else:
                    break
# for cluster
            if input_list['cluster_calculation']:
                try:
                    os.mkdir(os.path.join(Flame_dir,step_number,'divcheck','cluster'))
                except FileExistsError:
                    with open(log_file, 'a') as f:
                        f.write('>>> Cannot proceed. {} exists'.format(os.path.join(Flame_dir,step_number,'divcheck','cluster'))+'\n')
                    exit()
                if run_exists():
                    with open(log_file, 'a') as f:
                        f.write('>>> ERROR: unfinished jobs in Launchpad <<<'+'\n')
                    exit()
# get divcheck workflow
                flame_divcheck_c_wf = get_divcheck_c_wf(step_number)
# add workflow
                add_wf(flame_divcheck_c_wf)
# run train jobs
                if not run_jobs('divcheck-cluster',os.path.join(Flame_dir,step_number,'divcheck','cluster')):
                    with open(log_file, 'a') as f:
                        f.write('>>> Cannot proceed <<<'+'\n')
                    exit()
# wait until all jobs are done
                while True:
                    if run_exists():
                        fizzle_lostruns()
                        sleep(60)
                    else:
                        break
# check launchpad status
            lp_state = check_lp(['flame_divcheck-bulk_'+step_number, 'flame_divcheck-cluster_'+step_number])
            if lp_state == 'FIZZLED':
                with open(log_file, 'a') as f:
                    f.write('>>> WARNING: Workflow is FIZZLED <<<'+'\n')
            elif lp_state == 'unknown':
                with open(log_file, 'a') as f:
                    f.write('>>> Cannot proceed. The state of Workflow is not known.<<<'+'\n')
                exit()
#
            with open(log_file, 'a') as f:
                f.write('FLAME-{}: divcheck calculations ended.'.format(step_number)+'\n')
                f.write('End time: {}'.format(datetime.now())+'\n')
#
            FLAME_step_name = 'VASP_NSW0'
###FLAME VASP file preparation
        if FLAME_step_name == 'VASP_NSW0':
            with open(log_file, 'a') as f:
                f.write("-----------------------------------------------"+'\n')
                f.write('FLAME-{}: VASP single point calculations'.format(step_number)+'\n')
                f.write('Start time: {}'.format(datetime.now())+'\n')
# make directories
            try:
                os.mkdir(os.path.join(Flame_dir,step_number,'VASP_run'))
            except FileExistsError:
                pass
            try:
                os.mkdir(os.path.join(Flame_dir,step_number,'VASP_run','bulk'))
            except FileExistsError:
                with open(log_file, 'a') as f:
                    f.write('>>> Cannot proceed. {} exists'.format(os.path.join(Flame_dir,step_number,'VASP_run','bulk'))+'\n')
                exit()

            if run_exists():
                with open(log_file, 'a') as f:
                    f.write('>>> ERROR: unfinished jobs in Launchpad <<<'+'\n')
                exit()
#add structures from divcheck
            wfname_list = []
            with open(log_file, 'a') as f:
                f.write('Adding bulk structures from step-{}'.format(c_s_n)+'\n')
 
            for root, dirs, files in os.walk(os.path.join(Flame_dir,step_number,'divcheck', 'bulk')):
                if 'nat.dat' in files:
                    with open(os.path.join(root,'nat.dat'), 'r') as f:
                        nat = f.readline().strip()
                    if 'checked_position_force.json' in files:
                        with open(os.path.join(root,'checked_position_force.json') ,'r') as f:
                            confs = json.loads(f.read())
                        nsw0_structures = []
                        for conf in confs:
                            lattice = conf['conf']['cell']
                            crdnts = []
                            spcs = []
                            for coord in conf['conf']['coord']:
                                crdnts.append([coord[0],coord[1],coord[2]])
                                spcs.append(coord[3])
                            struct=Structure(lattice, spcs, crdnts, coords_are_cartesian=True)
                            nsw0_structures.append(struct.as_dict())
# get wf
                        vasp_nsw0_wf = get_vasp_wf(nsw0_structures, 'NSW0', wf_name = 'NSW0-'+str(nat)+'-atoms-bulk')
# add wf
                        add_wf(vasp_nsw0_wf)
                        wfname_list.append('NSW0-'+str(nat)+'-atoms-bulk')
                    else:
                        with open(log_file, 'a') as f:
                            f.write('no structure for single point calculation in {}'.format(root)+'\n')
#add stressed structures from poslows
            stressed_struct = []
            if c_s_n > 1:
                step_n = 'step-'+str(c_s_n-1)
                if os.path.exists(os.path.join(Flame_dir,step_n,'minhocao/poslow_structures.json')):
                    with open(os.path.join(Flame_dir,step_n,'minhocao/poslow_structures.json'), 'r') as f:
                        struct = json.loads(f.read())
                    for s in struct:
                        stressed_struct.extend(compress(Structure.from_dict(s)))
                        stressed_struct.extend(expand(Structure.from_dict(s)))
                        with open(log_file, 'a') as f:
                            f.write('Adding {} stressed structures with {} atoms from step {}'.format(len(stressed_struct), len(Structure.from_dict(s).sites), c_s_n-1)+'\n')
# get ws
                    vasp_stressed_wf = get_vasp_wf(stressed_struct, 'NSW0', wf_name = 'stressed-structs') 
# add wf
                    add_wf(vasp_stressed_wf)
                    wfname_list.append('stressed-structs')
# run jobs
            if not run_jobs('NSW0', os.path.join(Flame_dir,step_number,'VASP_run','bulk')):
                with open(log_file, 'a') as f:
                    f.write('>>> Cannot proceed <<<'+'\n')
                exit()
# wait until all jobs are done
            while True:
                if run_exists():
                    fizzle_lostruns()
                    sleep(60)
                else:
                    break
# for clusters 
            if input_list['cluster_calculation']:
                with open(log_file, 'a') as f:
                    f.write('Adding cluster structures from step-{}'.format(c_s_n)+'\n')
# make directories
                try:
                    os.mkdir(os.path.join(Flame_dir,step_number,'VASP_run','cluster'))
                except FileExistsError:
                    with open(log_file, 'a') as f:
                        f.write('>>> Cannot proceed. {} exists'.format(os.path.join(Flame_dir,step_number,'VASP_run','cluster'))+'\n')
                    exit()
            if run_exists():
                with open(log_file, 'a') as f:
                    f.write('>>> ERROR: unfinished jobs in Launchpad <<<'+'\n')
                exit()

#
                for root, dirs, files in os.walk(os.path.join(Flame_dir,step_number,'divcheck', 'cluster')):
                    if 'nat.dat' in files:
                        with open(os.path.join(root,'nat.dat'), 'r') as f:
                            nat = f.readline().strip()
                        if 'checked_position_force.json' in files:
                            with open(os.path.join(root,'checked_position_force.json') ,'r') as f:
                                confs = json.loads(f.read())
                            nsw0_structures = []
                            for conf in confs:
                                lattice = conf['conf']['cell']
                                crdnts = []
                                spcs = []
                                for coord in conf['conf']['coord']:
                                    crdnts.append([coord[0],coord[1],coord[2]])
                                    spcs.append(coord[3])
                                struct=Structure(lattice, spcs, crdnts, coords_are_cartesian=True)
                                nsw0_structures.append(struct.as_dict())
# get wf
                            vasp_nsw0_wf = get_vasp_wf(nsw0_structures, 'NSW0', wf_name = 'NSW0-'+str(nat)+'-atoms-cluster')
# add wf
                            add_wf(vasp_nsw0_wf)
                            wfname_list.append('NSW0-'+str(nat)+'-atoms-cluster')

                        else:
                            with open(log_file, 'a') as f:
                                f.write('no structure for single point calculation in {}'.format(root)+'\n')
# run jobs
                if not run_jobs('NSW0', os.path.join(Flame_dir,step_number,'VASP_run','cluster')):
                    with open(log_file, 'a') as f:
                        f.write('>>> Cannot proceed <<<'+'\n')
                    exit()
# wait until all jobs are done
                while True:
                    if run_exists():
                        fizzle_lostruns()
                        sleep(60)
                    else:
                        break
# check launchpad status
            lp_state = check_lp(wfname_list)
            if lp_state == 'FIZZLED':
                with open(log_file, 'a') as f:
                    f.write('>>> WARNING: Workflow is FIZZLED <<<'+'\n')
            elif lp_state == 'unknown':
                with open(log_file, 'a') as f:
                    f.write('>>> Cannot proceed. The state of Workflow is not known.<<<'+'\n')
                exit()
#
            with open(log_file, 'a') as f:
                f.write('FLAME-{}: VASP single point calculations ended.'.format(step_number)+'\n')
                f.write('End time: {}'.format(datetime.now())+'\n')
            FLAME_step_name = 'VASP_collecting_data'
###FLAME VASP_collecting_data
        if FLAME_step_name == 'VASP_collecting_data':
            with open(log_file, 'a') as f:
                f.write("-----------------------------------------------"+'\n')
                f.write('FLAME-{} VASP_collecting_data'.format(step_number)+'\n')
                f.write('Start time: {}'.format(datetime.now())+'\n')
            try:
                shutil.rmtree(os.path.join(Flame_dir,step_number,'task_files'))
            except:
                pass
            os.mkdir(os.path.join(Flame_dir,step_number,'task_files'))
#
            next_step_minhocao_structs = [] 
            for root, dirs, files in os.walk(os.path.join(Flame_dir,step_number,'VASP_run','bulk')):
                if 'task.json' in files: 
                    with open(os.path.join(root,'task.json'), 'r') as f:
                        s = json.loads(f.read())
                    epa = s['output']['energy_per_atom']
                    if float(epa) < 0:
                        struct = Structure.from_dict(s['output']['structure'])
                        forces = s['output']['forces']
                        new_task_name = "task" + str(epa) + '.json'
                        dir_name = str(len(struct.sites))+'_atoms'
                        if not os.path.exists(os.path.join(Flame_dir,step_number,'task_files', dir_name)):
                            os.mkdir(os.path.join(Flame_dir,step_number,'task_files', dir_name))
                        shutil.copyfile(os.path.join(root,'task.json'),\
                                        os.path.join(Flame_dir,step_number,'task_files',dir_name,new_task_name))
#select structures for next step minhocao
                        tot_forces = []
                        for j in range(0,len(forces)):
                            tot_forces.append(math.sqrt(forces[j][0]**2 + forces[j][1]**2 + forces[j][2]**2))
                        max_tot_foce = max(tot_forces)
                        if max_tot_foce < 0.11: 
                            next_step_minhocao_structs.append(struct.as_dict())
#write structures for next step minhocao
            if len(next_step_minhocao_structs)> 0:
                with open(os.path.join(Flame_dir,step_number,'minhocao','poslow_structures.json'),'w') as f:
                    json.dump(next_step_minhocao_structs,f)
# report
            with open(log_file, 'a') as f:
                f.write("-----------------"+'\n')
                f.write('Number of structures for the next step minhocao: {}'.format(len(next_step_minhocao_structs))+'\n')
# for clusters
            if input_list['cluster_calculation']:
                for root, dirs, files in os.walk(os.path.join(Flame_dir,step_number,'VASP_run','cluster')):
                    if 'task.json' in files:
                        with open(os.path.join(root,'task.json'), 'r') as f:
                            s = json.loads(f.read())
                        epa = s['output']['energy_per_atom']
                        if float(epa) < 0:
                            struct = Structure.from_dict(s['output']['structure'])
                            forces = s['output']['forces']
                            new_task_name = "task" + str(epa) + '.json'
                            dir_name = str(len(struct.sites))+'_atoms'
                            if not os.path.exists(os.path.join(Flame_dir,step_number,'task_files', dir_name)):
                                os.mkdir(os.path.join(Flame_dir,step_number,'task_files', dir_name))
                            shutil.copyfile(os.path.join(root,'task.json'),\
                                            os.path.join(Flame_dir,step_number,'task_files',dir_name,new_task_name))
#
            with open(log_file, 'a') as f:
                f.write('VASP collecting data ended.'+'\n')
                f.write('End time: {}'.format(datetime.now())+'\n')
#
            FLAME_step_name = 'train'
