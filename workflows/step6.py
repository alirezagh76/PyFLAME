import os
import sys
import shutil
import json
import yaml
from datetime import datetime
from sys import exit

from pymatgen import Structure
from config import *

def step_6():
    with open(log_file, 'a') as f:
        f.write("---------------------------------------------------------------------------------------------------"+'\n')
        f.write('STEP 6'+'\n')
        f.write('Start time: {}'.format(datetime.now())+'\n')
#creating directories
    try:
        os.mkdir(Flame_dir)
    except:
        pass
    try: 
        os.mkdir(os.path.join(Flame_dir,'step-0'))
    except:
        with open(log_file, 'a') as f:
            f.write('>>> Cannot procceed. {} exists. <<<'.format(os.path.join(Flame_dir,'step-0'))+'\n')
        exit()
    os.mkdir(os.path.join(Flame_dir,'step-0','task_files'))
#
    for root, dirs, files in os.walk(perturbed_bulk_structure_optimization_dir):
        if 'task.json' in files:
                try:
                    with open(os.path.join(root,'task.json'), 'r') as f:
                        s = json.loads(f.read())
                except:
                    with open(log_file, 'a') as f:
                        f.write('cannt open task file in {}'.format(root)+'\n')
                    continue
# remove high energy configurations
                epa = s['output']['energy_per_atom']
                if float(epa) < 0:
                    struct = Structure.from_dict(s['output']['structure'])
                    new_task_name = "task" + str(epa) + '.json'
                    dir_name = str(len(struct.sites))+'_atoms'
                    if not os.path.exists(os.path.join(Flame_dir,'step-0','task_files', dir_name)):
                        os.mkdir(os.path.join(Flame_dir,'step-0','task_files', dir_name))
                    shutil.copyfile(os.path.join(root,'task.json') ,os.path.join(Flame_dir,'step-0','task_files',dir_name,new_task_name))
# report
    for dirs in os.listdir(os.path.join(Flame_dir,'step-0','task_files')):
        list = os.listdir(os.path.join(Flame_dir,'step-0','task_files',dirs))
        with open(os.path.join(perturbed_bulk_structure_optimization_dir,'perturbed-bulk_stressed-structure.dat'), 'a') as f:
            f.write("number of structures for {}: {}".format(dirs,len(list))+'\n')
    with open(log_file, 'a') as f:
        f.write('STEP 6 ended.'+'\n')
        f.write('End time: {}'.format(datetime.now())+'\n')
    return steps_status[6] 
