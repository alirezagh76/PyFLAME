import os
import sys
from fireworks import Firework, PyTask

class ISCSPFW(Firework):
    def __init__(
       self,
       name,
       original_species,
       parents=None,
       **kwargs
    ):
        t = []
        t.append(PyTask(func='structure.ionic_substitution_csp.ionic_substitution_csp', args = [original_species]))

        super().__init__(t, parents = parents, name = name, **kwargs)

class CryspnetCSPFW(Firework):
    def __init__(
       self,
       name,
       formulas,
       space_groups,
       parents=None,
       **kwargs
    ):
        t = []
        t.append(PyTask(func='structure.cryspnet_csp.cryspnet_csp', args = [formulas, space_groups]))

        super().__init__(t, parents = parents, name = name, **kwargs)

class RandomStructFW(Firework):
    def __init__(
       self,
       name,
       compositions,
       dim,
       parents=None,
       **kwargs
    ):
        t = []
        t.append(PyTask(func='structure.random_structures.generate_random_structure', args = [compositions, dim] ))

        super().__init__(t, parents = parents, name = name, **kwargs)


