import io
import os
import sys
from sys import exit
from datetime import datetime
import json 
import yaml
from pymatgen.core.structure import Structure
from pymatgen.core.composition import Composition
from pymatgen.analysis.structure_matcher import StructureMatcher, ElementComparator
from pymatgen.transformations.standard_transformations import AutoOxiStateDecorationTransformation
from pymatgen.analysis.structure_prediction.substitution_probability import SubstitutionPredictor
from pymatgen.analysis.structure_prediction.substitutor import Substitutor
from pymatgen import MPRester

from config import *
from structure.io_db import get_known_structures

mpr = MPRester()

def ionic_substitution_csp(original_species):
    with open(os.path.join(bulk_structure_optimization_dir,'bulk_structure.dat'), 'a') as f:
        f.write('Ionic-substitution crystal structure prediction:'+'\n')
# known structures 
    known_structs_dict = get_known_structures(original_species)
# compute ionic substitution probabilities
    try:
        subs = SubstitutionPredictor(threshold=input_list['threshold']).list_prediction(original_species)
    except:
        with open(log_file, 'a') as f:
            f.write('>>> ERROR from substitution_probability: one of the species ({}) is not allowed for the probability model <<<'.format(original_species)+'\n')
        exit()
    subs.sort(key = lambda x: x['probability'], reverse = True)
    trial_subs = []
    trial_subs = [list(sub['substitutions'].keys()) for sub in subs]
    elem_sys_list = [[specie.element for specie in sub] for sub in trial_subs]
    chemsys_set = set()
# get the structures 
    for sys in elem_sys_list:
        chemsys_set.add("-".join(map(str,sys)))
    with open(log_file, 'a') as f:
        f.write('Getting all structures from MPD ...     {}'.format(datetime.now())+'\n')
    all_structs = {}
    for chemsys in chemsys_set:
        all_structs[chemsys] = mpr.get_structures(chemsys)
    with open(log_file, 'a') as f:
        f.write('Getting all structures from MPD is done {}'.format(datetime.now())+'\n')
#
    auto_oxi = AutoOxiStateDecorationTransformation()
    oxi_structs = {}
    for chemsys in all_structs:
        oxi_structs[chemsys] = []
        for num, struct in enumerate(all_structs[chemsys]):
            try:
                oxi_structs[chemsys].append({'structure': auto_oxi.apply_transformation(struct), 'id': str(chemsys + "_" + str(num))})
            except:
                continue 
# ionic substitution
    sbr = Substitutor(threshold = input_list['threshold']) 
    trans_structs = {}
    for chemsys in oxi_structs:
        trans_structs[chemsys] = sbr.pred_from_structures(original_species,oxi_structs[chemsys],remove_existing=True)
# filter based on number of atoms
    transformed_structures = []
    for chemsys in trans_structs:
        for struct in trans_structs[chemsys]:
            if len(struct.sites) <= input_list['max_atom_bulk'] and len(struct.sites) >= input_list['min_atom_bulk'] :
                transformed_structures.append(struct)
    with open(os.path.join(bulk_structure_optimization_dir,'bulk_structure.dat'), 'a') as f:
        f.write('Number of transformed structurres with number of atoms between {} and {} atoms: {}'\
               .format(input_list['min_atom_bulk'], input_list['max_atom_bulk'], len(transformed_structures))+'\n')
# create object for structure matching
    sm = StructureMatcher(comparator=ElementComparator(),primitive_cell=True)
# removing duplicated structures in transformed structures
    filtered_structs_1 = [] 
    seen_structs = []
    for struct1 in transformed_structures:
        found = False
        for struct2 in seen_structs:
            if sm.fit(struct1.final_structure, struct2):
                found = True
                break
        if not found:
            filtered_structs_1.append(struct1.final_structure)
            seen_structs.append(struct1.final_structure)
    with open(os.path.join(bulk_structure_optimization_dir,'bulk_structure.dat'), 'a') as f:
        f.write('Number of structures after removing duplicated structures in transformed structures: {}'.format(len(filtered_structs_1))+'\n')
# removing the duplicated structures compared with the original structures
    filtered_structs_2 = []
    for struct1 in filtered_structs_1:
        found = False
        for struct2 in known_structs_dict:
            if sm.fit(struct1, Structure.from_dict(struct2)):
                found = True
                break
        if not found:
            filtered_structs_2.append(struct1)
    with open(os.path.join(bulk_structure_optimization_dir,'bulk_structure.dat'), 'a') as f:
        f.write('Number of structures after removing duplicated structures in transformed structures and structurres in the MPDB: {}'.format(len(filtered_structs_2))+'\n')
# final structures
    final_structs_dict = []
    for struct in filtered_structs_2:
        final_structs_dict.append(struct.as_dict())
    final_structs_dict.extend(known_structs_dict)
    
# stro structures
    if len(final_structs_dict) == 0:
        with open(log_file, 'a') as f:
            f.write('>>> Cannot proceed. No bulk structure for optimixation <<<'+'\n')
            exit()
    else:
        with open(os.path.join(bulk_structure_optimization_dir,'all_bulk_structures.json'),'w') as f:
            json.dump(final_structs_dict, f)
        with open(log_file, 'a') as f:
            f.write('Number of bulk structures predicted by ioninc-substitution crystal structure prediction: {}'.format(len(final_structs_dict))+'\n')

# fixed_stoichiomety? 
    if input_list['fixed_stoichiometry']:
        fixed_stoichiometry_structures = []
        if input_list['Composition'][0]['MP_ID']:
            original_formula = mpr.query(criteria={"task_id": input_list['Composition'][0]['MP_ID']}, properties=['pretty_formula'])
            for s in final_structs_dict:
                if Composition(Structure.from_dict(s).formula).reduced_formula == Composition(original_formula[0]['pretty_formula']).reduced_formula:
                    fixed_stoichiometry_structures.append(s)
            if len(fixed_stoichiometry_structures) == 0:
                with open(log_file, 'a') as f:
                    f.write('>>> Cannot proceed. No bulk structure for optimixation (fixed stoichiometry) <<<'+'\n')
                exit()
            else:
                with open(os.path.join(bulk_structure_optimization_dir,'fixed_stoichiometry_bulk_structures.json'), 'w') as f:
                    json.dump(fixed_stoichiometry_structures, f)
                with open(log_file, 'a') as f:
                    f.write('Number of bulk structures with the same toichiometry after ioninc-substitution crystal structure prediction: {}'.\
                    format(len(fixed_stoichiometry_structures))+'\n')
        else:
            with open(log_file, 'a') as f:
                f.write('>>> Cannot proceeed. Fixed stoichiometry is not possible <<<'+'\n')
            exit()
