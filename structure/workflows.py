import os
import sys
from fireworks import Workflow

from structure.fireworks import *
from config import *

def get_i_s_csp_wf(o_s):
    wf_name = 'csp'
    i_s_csp_fw = [ISCSPFW(name = 'i_s_csp', original_species = o_s)]
    i_s_csp_wf = Workflow(i_s_csp_fw, name = wf_name)

    return i_s_csp_wf


def get_cryspnet_csp_wf(frmls, sgs):
    wf_name = 'csp'
    cryspnet_csp_fw = [CryspnetCSPFW(name = 'cryspnet_csp', formulas = frmls, space_groups = sgs)]
    cryspnet_csp_wf = Workflow(cryspnet_csp_fw, name = wf_name)

    return cryspnet_csp_wf

def get_random_struct_wf(comps, d):
    if d == 0:
        wf_name = 'random_cluster'
    if d == 3:
        wf_name = 'random_bulk'
    r_s_fw = [RandomStructFW(name = 'random_struct', compositions = comps, dim = d)]
    r_s_wf = Workflow(r_s_fw, name = wf_name)

    return r_s_wf

