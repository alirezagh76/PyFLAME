import io
import os
import sys
from sys import exit
from datetime import datetime
import yaml
import json 
from random import sample
from pymatgen.core.structure import Structure
from pymatgen.core.composition import Composition
from pymatgen.core.periodic_table import Element, Specie

from config import *

def user_ion_substitution():
    with open(os.path.join(bulk_structure_optimization_dir,'bulk_structure.dat'), 'a') as f:
        f.write('User ion exchange:'+'\n')
# read structures
    final_sub_structures_dict = []
    if input_list['fixed_stoichiometry']:
        try:
            with open(os.path.join(bulk_structure_optimization_dir,'fixed_stoichiometry_bulk_structures.json'), 'r') as f:
                all_s = json.loads(f.read())
        except:
            with open(log_file, 'a') as f:
                f.write('>>> Cannot proceed. Cannot find bulk structure files. <<<'+'\n')
            exit()
    else:
        try:
            with open(os.path.join(bulk_structure_optimization_dir,'all_bulk_structures.json'),'r') as f:
                all_s = json.loads(f.read())
        except:
            with open(log_file, 'a') as f:
                f.write('>>> Cannot proceed. Cannot find bulk structure files. <<<'+'\n')
            exit()
# loop over all bulk structures
    for s in all_s:
        crdnts = []
        spcs = []
        spcs_ind = []
# find the indices of the elemnt to be replaced
        for i in range(len(s['sites'])):
            spcs.append(s['sites'][i]['species'][0]['element'])
            crdnts.append(s['sites'][i]['xyz'])
            if(s['sites'][i]['species'][0]['element']) == input_list['first_specie'][0]:
                spcs_ind.append(i)
# the percentage to be replaced
        per = len(spcs_ind) * (float(input_list['percentage'])/100)
        if abs(round(per)- per) < 0.0001:
            ind_to_be_replaced = sample(spcs_ind, round(per))
            for intbr in ind_to_be_replaced:
                spcs[intbr] = Specie(input_list['second_specie'][0], input_list['second_specie'][1])
            try:
                sub_s=Structure(Structure.from_dict(s).lattice, spcs, crdnts, coords_are_cartesian=True, validate_proximity=False)
                final_sub_structures_dict.append(sub_s.as_dict())
                with open(os.path.join(bulk_structure_optimization_dir,'bulk_structure.dat'), 'a') as f:
                    f.write('{}% of {} are replaced with {} in {}'.format(input_list['percentage'], input_list['first_specie'][0],\
                    input_list['second_specie'][0], Structure.from_dict(s).formula)+'\n')
            except:
                with open(os.path.join(bulk_structure_optimization_dir,'bulk_structure.dat'), 'a') as f:
                    f.write('It is not possible to replace {} with {} in {}'.format(input_list['first_specie'][0],input_list['second_specie'][0],\
                    Structure.from_dict(s).formula)+'\n')
        else:
            with open(os.path.join(bulk_structure_optimization_dir,'bulk_structure.dat'), 'a') as f:
                f.write('It is not possible to replace {}% of {} atoms in {}'.format(input_list['percentage'], input_list['first_specie'][0],\
                Structure.from_dict(s).formula)+'\n')
#save sub structures
        if input_list['fixed_stoichiometry']:
            with open(os.path.join(bulk_structure_optimization_dir,'fixed_stoichiometry_bulk_structures.json'), 'w') as f:
                json.dump(final_sub_structures_dict, f)
        else:
            with open(os.path.join(bulk_structure_optimization_dir,'all_bulk_structures.json'),'w') as f:
                json.dump(final_sub_structures_dict, f)
#modify element list
    if len(final_sub_structures_dict) > 0:
        elmnt_list = []
        with open(os.path.join(bulk_structure_optimization_dir,'elements.dat'), 'r') as f:
            elmnt_list = [line.strip() for line in f]
        if input_list['percentage'] == 100:
            for index, item in enumerate(elmnt_list):
                if item == input_list['first_specie'][0]:
                    elmnt_list[index] = input_list['second_specie'][0]
        else:
            elmnt_list.append(input_list['second_specie'][0])
        with open(os.path.join(bulk_structure_optimization_dir,'elements.dat'), 'w') as f:
            f.writelines(["%s\n" % el  for el in elmnt_list])
        with open(log_file, 'a') as f:
                    f.write('{}% of {} is replaced with {} in {} structures'.format(input_list['percentage'], input_list['first_specie'][0],\
                    input_list['second_specie'][0],len(final_sub_structures_dict))+'\n')
    else:
        with open(log_file, 'a') as f:
            f.write('WARNING: no ion substitution is performed'+'\n')            
