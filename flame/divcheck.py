import os
import sys
import shutil
import re
import math
import json
import yaml
from yaml import Loader 
from collections import defaultdict

from pymatgen.core.structure import Structure

from structure.core import min_distance
from structure.io_db import get_element_list
from flame.core import *
from config import *

import flame.flame_functions.atoms
from flame.flame_functions.ascii import *
from flame.flame_functions.io_yaml import *
from flame.flame_functions.vasp import *
from flame.flame_functions.latvec2dproj import *

def write_divcheck_b_files(step_number, nat):
#for minhopp structures

#for minhocao structures
    with open(os.path.join(Flame_dir,step_number,'minhocao','minhocao-'+step_number+'.json'), 'r') as f:
        minhocao_confs = json.loads(f.read())
    for a_conf in minhocao_confs[nat]:
        a_conf['conf']['force'] = False
        a_conf['conf']['epot'] = -1 # it is a new structure
        a_conf['conf']['bc'] = 'bulk'
        write_p_f_from_yaml(a_conf,'position_force_divcheck.yaml')
#write SE.ann.input.yaml
    elmnt_list = get_element_list()
    write_SE_ann_FLAME(elmnt_list)

#write flame_in.yamli for divcheck
    with open('flame_in.yaml', 'w') as f:
        f.write('main:'+'\n')
        f.write('    task: ann'+'\n')
        f.write('    types:')
        for elmnt in elmnt_list:
            f.write(' {}'.format(elmnt))
        f.write('\n'+'\n')
        f.write('ann:'+'\n')
        f.write('    subtask: check_symmetry_function'+'\n')
        f.write('    etol: 1.E-4'+'\n')
        f.write('    dtol: 25.E-2'+'\n')
        f.write('    normalization: True'+'\n')
        f.write('    read_forces: False'+'\n')

#write list_posinp_check.yaml for divcheck
    with open('list_posinp_check.yaml', 'w') as f:
        f.write('files:'+'\n')
        f.write(' - position_force_divcheck.yaml'+'\n')
# extra file for nat 
    with open('nat.dat', 'w') as f:
        f.write(nat)

def write_divcheck_c_files(step_number, nat):
#for minhopp structures

#for minhocao structures
    with open(os.path.join(Flame_dir,step_number,'minhopp','minhopp-'+step_number+'.json'), 'r') as f:
        minhocao_confs = json.loads(f.read())
    for a_conf in minhocao_confs[nat]:
        a_conf['conf']['force'] = False
        a_conf['conf']['epot'] = -1 # it is a new structure
        a_conf['conf']['bc'] = 'free'
        write_p_f_from_yaml(a_conf,'position_force_divcheck.yaml')
#write SE.ann.input.yaml
    elmnt_list = get_element_list()
    write_SE_ann_FLAME(elmnt_list)

#write flame_in.yamli for divcheck
    with open('flame_in.yaml', 'w') as f:
        f.write('main:'+'\n')
        f.write('    task: ann'+'\n')
        f.write('    types:')
        for elmnt in elmnt_list:
            f.write(' {}'.format(elmnt))
        f.write('\n'+'\n')
        f.write('ann:'+'\n')
        f.write('    subtask: check_symmetry_function'+'\n')
        f.write('    etol: 1.E-4'+'\n')
        f.write('    dtol: 25.E-2'+'\n')
        f.write('    normalization: True'+'\n')
        f.write('    read_forces: False'+'\n')

#write list_posinp_check.yaml for divcheck
    with open('list_posinp_check.yaml', 'w') as f:
        f.write('files:'+'\n')
        f.write(' - position_force_divcheck.yaml'+'\n')
# extra file for nat 
    with open('nat.dat', 'w') as f:
        f.write(nat)

def run_pickdiff(step_number, nat):
# d_tol
    dtol_prefactor = input_list['dtol_prefactor']
    with open(os.path.join(Flame_dir,'ave_dis','ave_dis.json'), 'r') as f:
        ave_dis_dict = yaml.load(f, Loader=yaml.FullLoader)
    s_n = int(re.split('-',step_number)[1]) - 1
    dtol = float(ave_dis_dict[str(nat)])*float(dtol_prefactor[s_n])
#
    if os.path.exists('distall'):
        dist=[]
        f=open('distall','r')
        nn=-1
        nconf=0
        for line in f.readlines():
            if not int(line.split()[1])==nn:
                nconf+=1
                dist.append([])
                for iconf in range(nconf-1):
                    dist[-1].append(dist[iconf][nconf-1])
                dist[-1].append(0.0)
                nn=int(line.split()[1])
            dist[-1].append(float(line.split()[4]))
        f.close()
        nconf+=1
        dist.append([])
        for iconf in range(nconf-1):
            dist[-1].append(dist[iconf][nconf-1])
        dist[-1].append(0.0)

        sel=[]
        for iconf in range(nconf):
            if iconf==0:
                sel.append(0) #This is the first configuration
                continue
            new=True
            for jconf in range(len(sel)):
                if dist[iconf][sel[jconf]]<dtol:
                    new=False
                    break
            if new==True:
                sel.append(iconf)
        atoms_all=read_yaml('position_force_divcheck.yaml')
        atoms_all_out=[]
        for iconf in range(len(sel)):
            atoms_all_out.append(Atoms)
            atoms_all_out[-1]=copy.deepcopy(atoms_all[sel[iconf]])
        atoms_all_yaml = []
        json_dump = []
        for atoms in atoms_all_out:
            dict_atoms=atoms2dict(atoms)
            if dict_atoms['conf']['epot'] == -1: #only new structures
                atoms_all_yaml.append(atoms)
                json_dump.append(dict_atoms)
        with open('checked_position_force.json', 'w') as f:
            json.dump(json_dump, f)

        write_yaml(atoms_all_yaml, 'checked_position_force.yaml')
    else:
        with open(log_file, 'a') as f:
            f.write('WARNING: no distall in {}'.format(os.getcwd())+'\n')

