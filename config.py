import os
import yaml

CONFIG_DIR = 'path_to_atomate_config'

CWD = 'path_of_the_current_directory'

OUTPUT_FOLDER = os.path.join(CWD,'OUTPUT')

input_file =   os.path.join(OUTPUT_FOLDER,'input.yaml')
restart_file = os.path.join(OUTPUT_FOLDER,'restart.yaml')
log_file =     os.path.join(OUTPUT_FOLDER,'pyflame.log')

job_script_file = os.path.join(CWD,'workflows/job_script/job_script.yaml')

bulk_structure_optimization_dir =           os.path.join(OUTPUT_FOLDER,'bulk_optimization')
perturbed_bulk_structure_optimization_dir = os.path.join(OUTPUT_FOLDER,'perturbed-bulk_cluster_optimization')
additional_structures_dir =                 os.path.join(OUTPUT_FOLDER,'additional_structures')
Flame_dir =                                 os.path.join(OUTPUT_FOLDER,'FLAME_calculations')

with open(input_file, 'r') as f:
    input_list = yaml.load(f, Loader=yaml.FullLoader)

steps_status = [None, True, True, True, True, True, True, True]
with open(restart_file, 'r') as f:
    restart = yaml.load(f, Loader=yaml.FullLoader)
if restart['stop_after_step'] >= 0:
    steps_status[restart['stop_after_step']] = False

